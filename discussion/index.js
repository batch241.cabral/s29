db.users.insertMany([
    {
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com",
        },
        courses: ["CSS", "Javascript", "Python"],
        department: "HR",
    },
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com",
        },
        courses: ["Python", "React", "PHP"],
        department: "HR",
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com",
        },
        courses: ["React", "Laravel", "Sass"],
        department: "HR",
    },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com",
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active",
    },
]);

db.users.find({ age: { $gt: 65 } });
db.users.find({ age: { $gte: 65 } });

db.users.find({ age: { $lt: 65 } });
db.users.find({ age: { $lte: 65 } });

db.users.find({ age: { $ne: 65 } });

db.users.find(
    {
        $and: [
            {
                age: { $gt: 65 },
            },
            {
                age: { $lt: 75 },
            },
        ],
    },
    {
        age: 1,
    }
);

db.users.find(
    { $and: [{ age: { $gt: 65 } }, { age: { $lt: 75 } }] },
    {
        age: 1,
    }
);
